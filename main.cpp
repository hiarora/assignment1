#include "mbed.h"
 
Timer timer;
DigitalOut led1(LED1);
DigitalOut led2(LED2);
DigitalOut led3(LED3);
DigitalOut led4(LED4);
int on = 1;
int begin, end, led = 1;

int main() {
    timer.start();
    begin = timer.read_ms();
    while(on){
        end = timer.read_ms();
        if(end - begin > 250){
            begin = timer.read_ms();
            switch(led){
                case 1:
                    led4 = 0;
                    led1 = 1;
                    led = 2;
                    break;
                case 2:
                    led1 = 0;
                    led2 = 1;
                    led = 3;
                    break;
                case 3:
                    led2 = 0;
                    led3 = 1;
                    led = 4;
                    break;
                case 4:
                    led3 = 0;
                    led4 = 1;
                    led = 1;
                    break;
            }
        }
    }
}